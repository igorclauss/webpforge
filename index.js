const imagemin = require("imagemin");
const imageminWebp = require("imagemin-webp");

imagemin(["./input/*.{jpg,png}"], {
  destination: "./output/",
  plugins: [
    imageminWebp({
      quality: 74
    }),
  ],
}).then(() => {
  console.log("Images Converted Successfully!!!");
});